﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace ExtendedReports.Data.LocalDb
{
    public class LocalDBContext: DbContext
    {
        public DbSet<LastUpdates> lastUpdates { get; set; }
        public DbSet<DateVariables> dateVariables { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=dateVariables.db");
        }
    }
}
