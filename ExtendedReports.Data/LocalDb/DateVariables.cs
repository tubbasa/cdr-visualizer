﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ExtendedReports.Data.LocalDb
{
    public class DateVariables
    {
        
        public int ID { get; set; }

        [Required]
        public DateTime dateTime { get; set; }
    }
}
