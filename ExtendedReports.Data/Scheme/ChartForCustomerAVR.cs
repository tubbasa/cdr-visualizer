﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExtendedReports.Data.Scheme
{
    public class ChartForCustomerAVR
    {
        public List<string> dates{ get; set; }
        public List<decimal> datas { get; set; }
        public string customerName { get; set; }
        public long customerNo{ get; set; }
        public string Ip { get; set; }
    }
}
