﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtendedReports.Data.Scheme
{
    public class Context
    {
        public List<Customers> customers { get; set; }
        public List<Devices> devices { get; set; }
        public Report reports { get; set; }
        public DateAndData datesAndDatas { get; set; }
        public List<System.Threading.Tasks.Task<ExtendedReports.Data.Scheme.ChartForCustomerAVR>> customerReports { get; set; }
        public List<ChartForIps> ipsReports { get; set; }
    }
}
