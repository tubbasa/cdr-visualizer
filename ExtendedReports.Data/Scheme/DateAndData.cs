﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtendedReports.Data.Scheme
{
    public class DateAndData
    {
        public List<string> dates { get; set; }
        public List<decimal> datas { get; set; }
    }
}
