﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtendedReports.Data
{
    public class ReportLine
    {
        public DateTime dataTime { get; set; }
        public string customerName { get; set; }
        public string deviceIp { get; set; }
        public long customerNumber { get; set; }
        public decimal amount { get; set; }

    }
}
