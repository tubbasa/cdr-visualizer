﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtendedReports.Data.Scheme
{
    public class Devices
    {
        public string deviceIp { get; set; }
        public long relatedCustomerId { get; set; }
        public List<ReportLine> reports { get; set; }
    }
}
