﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExtendedReports.Data.Scheme
{
    public class ChartForIps
    {
        public List<ChartForCustomerAVR> charts { get; set; }
    }
}
