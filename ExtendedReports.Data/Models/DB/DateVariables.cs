﻿using System;
using System.Collections.Generic;

namespace ExtendedReports.Data.Models.DB
{
    public partial class DateVariables
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
    }
}
