﻿using System;
using System.Collections.Generic;

namespace ExtendedReports.Data.Models.DB
{
    public partial class ReportsByCountAndDate
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public int? CustomerNo { get; set; }
        public DateTime? Date { get; set; }
        public string DeviceIp { get; set; }
        public decimal? Value { get; set; }
    }
}
