﻿using System;
using System.Collections.Generic;

namespace ExtendedReports.Data.Models.DB
{
    public partial class Customers
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
    }
}
