﻿using System;
using System.Collections.Generic;

namespace ExtendedReports.Data.Models.DB
{
    public partial class FileDetails
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public DateTime? LastEditedDate { get; set; }
        public DateTime? CreatedTime { get; set; }
    }
}
