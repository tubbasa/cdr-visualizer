﻿using System;
using System.Collections.Generic;

namespace ExtendedReports.Data.Models.DB
{
    public partial class ReadLines
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public long CustomerNo { get; set; }
        public DateTime Date { get; set; }
        public string DeviceIp { get; set; }
        public decimal Value { get; set; }
        public int? FileId { get; set; }
    }
}
