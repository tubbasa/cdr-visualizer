﻿using System;
using System.Collections.Generic;

namespace ExtendedReports.Data.Models.DB
{
    public partial class ReadDetails
    {
        public int Id { get; set; }
        public bool? IsCompleted { get; set; }
        public DateTime? LastEditTime { get; set; }
    }
}
