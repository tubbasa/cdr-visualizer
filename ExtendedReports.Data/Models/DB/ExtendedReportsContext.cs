﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ExtendedReports.Data.Models.DB
{
    public partial class ExtendedReportsContext : DbContext
    {
        public ExtendedReportsContext()
        {
        }

        public ExtendedReportsContext(DbContextOptions<ExtendedReportsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<DateVariables> DateVariables { get; set; }
        public virtual DbSet<Devices> Devices { get; set; }
        public virtual DbSet<FileDetails> FileDetails { get; set; }
        public virtual DbSet<ReadDetails> ReadDetails { get; set; }
        public virtual DbSet<ReadLines> ReadLines { get; set; }
        public virtual DbSet<ReportsByCountAndDate> ReportsByCountAndDate { get; set; }
        public virtual DbSet<ReportsByJustDate> ReportsByJustDate { get; set; }
        private string connectionString = ExtendedReports.Data.Extensions.GetConnectionString.GetConnectionStringFromAppJSON();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.EnableSensitiveDataLogging();
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.Property(e => e.CustomerName).HasMaxLength(300);
            });

            modelBuilder.Entity<DateVariables>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Date).HasColumnType("datetime");
            });

            modelBuilder.Entity<Devices>(entity =>
            {
                entity.Property(e => e.DeviceIp)
                    .HasColumnName("DeviceIP")
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<FileDetails>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.FileName).HasMaxLength(500);

                entity.Property(e => e.LastEditedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ReadDetails>(entity =>
            {
                entity.Property(e => e.LastEditTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<ReadLines>(entity =>
            {
                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.DeviceIp).HasMaxLength(50);

                entity.Property(e => e.Value).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<ReportsByCountAndDate>(entity =>
            {
                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.DeviceIp).HasMaxLength(50);

                entity.Property(e => e.Value).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<ReportsByJustDate>(entity =>
            {
                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.DeviceIp).HasMaxLength(50);

                entity.Property(e => e.Value).HasColumnType("decimal(18, 2)");
            });
        }
    }
}
