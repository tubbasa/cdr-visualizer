﻿using System;
using System.Collections.Generic;

namespace ExtendedReports.Data.Models.DB
{
    public partial class Devices
    {
        public int Id { get; set; }
        public string DeviceIp { get; set; }
    }
}
