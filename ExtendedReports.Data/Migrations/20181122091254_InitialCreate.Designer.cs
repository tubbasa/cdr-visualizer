﻿// <auto-generated />
using System;
using ExtendedReports.Data.LocalDb;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ExtendedReports.Data.Migrations
{
    [DbContext(typeof(LocalDBContext))]
    [Migration("20181122091254_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024");

            modelBuilder.Entity("ExtendedReports.Data.LocalDb.DateVariables", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("dateTime");

                    b.HasKey("ID");

                    b.ToTable("dateVariables");
                });

            modelBuilder.Entity("ExtendedReports.Data.LocalDb.LastUpdates", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("LastUpdateTime");

                    b.HasKey("ID");

                    b.ToTable("lastUpdates");
                });
#pragma warning restore 612, 618
        }
    }
}
