﻿using ExtendedReports.Data;
using ExtendedReports.Data.Models.DB;
using ExtendedReports.Data.Scheme;
using ExtendedReports.UI.Controllers;
using ExtendedReports.UI.Models;
using ExtendedReports.UI.Models.ExtraClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExtendedReports.UI.Helpers
{
    public static class FillDate
    {

        public static List<ChartForCustomerAVR> testList = new List<ChartForCustomerAVR>();
        public static ChartForCustomerAVR CheckDatesForCustomerAVR(DateTime firstDate, DateTime LastDate)
        {
            ChartForCustomerAVR chartList = new ChartForCustomerAVR();
            chartList.dates = new List<string>();
            chartList.datas = new List<decimal>();
            var dateCount = (LastDate - firstDate).TotalDays;
            for (int i = 0; i < dateCount; i++)
            {
                var date = firstDate.AddDays(i);
                for (int j = 0; j < 24; j++)
                {
                    chartList.dates.Add(date.AddHours(j).ToString());
                    chartList.datas.Add(0);
                }
            }

            return chartList;
        }
        public static ChartForCustomerAVR CheckDatesForCustomerAVR(DateTime? firstDate, DateTime? LastDate, List<DateDataModel> dateDataModels)
        {
            ChartForCustomerAVR chartList = new ChartForCustomerAVR();
            chartList.dates = new List<string>();
            chartList.datas = new List<decimal>();
            var dateCount = (LastDate - firstDate).Value.TotalDays;
            for (int i = 0; i < dateCount; i++)
            {
                var date = firstDate.Value.AddDays(i);
                for (int j = 0; j < 24; j++)
                {
                    var newDateTime = date.AddHours(j);
                    chartList.dates.Add(newDateTime.ToString());
                    if (dateDataModels.Exists(x => x.Date == newDateTime))
                    {
                        var selectedDateDataModels = dateDataModels.FirstOrDefault(x => x.Date == newDateTime);
                        chartList.datas.Add(selectedDateDataModels.Data.Value);
                    }
                    else
                    {
                        chartList.datas.Add(0);
                    }

                }
            }

            return chartList;
        }
        public static ChartForIps CheckDatesForSelectedIps(List<MultipleIpDates> dates)
        {
            ChartForIps chartModel = new ChartForIps();
            chartModel.charts = new List<ChartForCustomerAVR>();
            foreach (var item in dates)
            {
                ChartForCustomerAVR forOneIP = new ChartForCustomerAVR();
                forOneIP.datas = new List<decimal>();
                forOneIP.dates = new List<string>();
                forOneIP.customerNo = item.customerNo;
                forOneIP.Ip = item.ip;

                var dateCount = (item.endTime - item.startTime).TotalDays;
                for (int i = 0; i < dateCount; i++)
                {
                    var date = item.startTime.AddDays(i);
                    for (int j = 0; j < 24; j++)
                    {
                        forOneIP.dates.Add(date.AddHours(j).ToString());
                        forOneIP.datas.Add(0);
                    }
                }
                chartModel.charts.Add(forOneIP);
            }
            return chartModel;
        }
        public static ChartForIps CheckDatesForSelectedIps(DateTime? firstDate, DateTime? LastDate, string[] ips, ExtendedReportsContext context)
        {
            var ipler = ips[0].Split(',');
            ChartForIps MainChartModel = new ChartForIps();
            MainChartModel.charts = new List<ChartForCustomerAVR>();
            foreach (var item in ipler)
            {
                var dd = context.ReadLines.Where(d => d.DeviceIp == item).Sum(x => x.Value);
                var datas = (from x in context.ReadLines
                             where x.DeviceIp == context.Devices.FirstOrDefault(dev => dev.DeviceIp == item).DeviceIp
                             group new { x.Date, x.DeviceIp, x.Value } by x.Date
              ).Select(x => new DateDataModel { Date = x.Key, Data = x.Sum(k => k.Value), deviceIp = item }).ToList();
                ChartForCustomerAVR chartList = new ChartForCustomerAVR();
                chartList.dates = new List<string>();
                chartList.datas = new List<decimal>();
                chartList.Ip = item;
                var dateCount = (LastDate - firstDate).Value.TotalDays;
                for (int i = 0; i < dateCount; i++)
                {

                    var date = firstDate.Value.AddDays(i);
                    for (int j = 0; j < 24; j++)
                    {
                        var newDateTime = date.AddHours(j);
                        chartList.dates.Add(newDateTime.ToString());
                        if (datas.Exists(x => x.Date == newDateTime))
                        {
                            var selectedDateDataModels = datas.FirstOrDefault(x => x.Date == newDateTime);
                            chartList.datas.Add(selectedDateDataModels.Data.Value);
                        }
                        else
                        {
                            chartList.datas.Add(0);
                        }

                    }
                }
                MainChartModel.charts.Add(chartList);
            }

            return MainChartModel;
        }
        public static List<ChartForCustomerAVR> CheckDatesForCustomerPort(DateTime? firstDate, DateTime? LastDate, int customerId, ExtendedReportsContext context)
        {
            var datas = (from x in context.ReadLines
                         where x.CustomerName == context.Customers.FirstOrDefault(cust => cust.Id == customerId).CustomerName
                         group new { x.Date, x.DeviceIp, x.Value } by x.Date
          ).Select(x => new DateDataModel { Date = x.Key, Data = x.Sum(k => k.Value), customerName = context.Customers.FirstOrDefault(cst => cst.Id == customerId).CustomerName }).ToList();
            List<ChartForCustomerAVR> chartList = new List<ChartForCustomerAVR>();
            ChartForCustomerAVR chart = new ChartForCustomerAVR();
            chart.dates = new List<string>();
            chart.datas = new List<decimal>();
            chart.customerName = context.Customers.FirstOrDefault(x => x.Id == customerId).CustomerName;
            var dateCount = (LastDate - firstDate).Value.TotalDays;
            for (int i = 0; i < dateCount; i++)
            {

                var date = firstDate.Value.AddDays(i);
                for (int j = 0; j < 24; j++)
                {
                    var newDateTime = date.AddHours(j);
                    chart.dates.Add(newDateTime.ToString());
                    if (datas.Exists(x => x.Date == newDateTime))
                    {
                        var selectedDateDataModels = datas.FirstOrDefault(x => x.Date == newDateTime);
                        chart.datas.Add(selectedDateDataModels.Data.Value);
                    }
                    else
                    {
                        chart.datas.Add(0);
                    }

                }
                chartList.Add(chart);
            }
            return chartList;
        }
        public static ChartForIps CheckDatesForIpsPort(DateTime? firstDate, DateTime? LastDate, List<string> ips, ExtendedReportsContext context)
        {

            ChartForIps MainChartModel = new ChartForIps();
            MainChartModel.charts = new List<ChartForCustomerAVR>();
            foreach (var item in ips)
            {
                var dd = context.ReadLines.Where(d => d.DeviceIp == item).Sum(x => x.Value);
                var datas = context.ReadLines.Where(x => x.DeviceIp == item).ToList();
                ChartForCustomerAVR chartList = new ChartForCustomerAVR();
                chartList.dates = new List<string>();
                chartList.datas = new List<decimal>();
                chartList.Ip = item;
                var dateCount = (LastDate - firstDate).Value.TotalDays;
                for (int i = 0; i < dateCount; i++)
                {

                    var date = firstDate.Value.AddDays(i);
                    for (int j = 0; j < 24; j++)
                    {
                        var newDateTime = date.AddHours(j);
                        chartList.dates.Add(newDateTime.ToString());
                        if (datas.Exists(x => x.Date == newDateTime))
                        {
                            var selectedDateDataModels = datas.Where(x => x.Date == newDateTime);
                            chartList.datas.Add(selectedDateDataModels.Count());
                        }
                        else
                        {
                            chartList.datas.Add(0);
                        }

                    }
                }
                MainChartModel.charts.Add(chartList);
            }

            return MainChartModel;
        }
        public static ChartForIps CheckDatesForCustomersPort(DateTime? firstDate, DateTime? LastDate, long? customerId, ExtendedReportsContext context)
        {

            ChartForIps MainChartModel = new ChartForIps();
            MainChartModel.charts = new List<ChartForCustomerAVR>();
            var selectedCustomer = context.Customers.FirstOrDefault(d => d.Id == customerId);
            var datas = context.ReadLines.Where(x => x.CustomerName == selectedCustomer.CustomerName).ToList();
            ChartForCustomerAVR chartList = new ChartForCustomerAVR();
            chartList.dates = new List<string>();
            chartList.datas = new List<decimal>();
            chartList.customerName = selectedCustomer.CustomerName;
            var dateCount = (LastDate - firstDate).Value.TotalDays;
            for (int i = 0; i < dateCount; i++)
            {

                var date = firstDate.Value.AddDays(i);
                for (int j = 0; j < 24; j++)
                {
                    var newDateTime = date.AddHours(j);
                    chartList.dates.Add(newDateTime.ToString());
                    if (datas.Exists(x => x.Date == newDateTime))
                    {
                        var selectedDateDataModels = datas.Where(x => x.Date == newDateTime);
                        chartList.datas.Add(selectedDateDataModels.Count());
                    }
                    else
                    {
                        chartList.datas.Add(0);
                    }

                }
            }
            MainChartModel.charts.Add(chartList);

            return MainChartModel;
        }
        public static DateAndData FillDateForTheBeginning(DateTime? firstDate, DateTime? LastDate)
        {


            DateAndData dateData = new DateAndData();
            dateData.datas = new List<decimal>();
            dateData.dates = new List<string>();
            var dateCount = (LastDate.Value.DayOfYear - firstDate.Value.DayOfYear) + 1;

            DateTime date = firstDate.Value;

            while (date < LastDate)
            {
                date = date.AddHours(1);
                dateData.datas.Add(0);
                dateData.dates.Add(date.ToString());
            }
            var last = dateData.dates.Last();
            return dateData;


        }


    }


}
