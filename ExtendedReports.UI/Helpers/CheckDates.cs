﻿using ExtendedReports.Data.LocalDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExtendedReports.UI.Helpers
{
    public class CheckDates
    {
       public bool checkDb()
        {
            using (var db = new LocalDBContext())
            {
                var lastUpdatedDate = db.lastUpdates.FirstOrDefault().LastUpdateTime;
                var thisYear = DateTime.Now.Year;
                if (lastUpdatedDate.Year != thisYear)
                {
                    var calculatedDates = fillDbWithDates();
                    //db.dateVariables.AddRangeAsync(calculatedDates);
                    //insert all dates 
                }


           
             
            }
            return true;
        }

        public List<DateTime> fillDbWithDates()
        {
            var thisYear = DateTime.Now.Year;
            DateTime firstDay = new DateTime(thisYear, 1, 1);
            DateTime lastDay = new DateTime(thisYear, 12, 31);
            List<DateTime> dates = new List<DateTime>();
            var dayCount = (lastDay - firstDay).TotalDays;
            for (int i = 0; i < dayCount; i++)
            {
                var date = firstDay.AddDays(i);
                for (int j = 0; j < 24; j++)
                {
                    var insertedDate = date.AddHours(j);
                    dates.Add(insertedDate);

                }
                //foreach (var hour in hours)
                //{

                //}
            }

            return dates;
        }

    }
}
