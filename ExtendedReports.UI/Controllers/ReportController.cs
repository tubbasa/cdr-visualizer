﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ExtendedReports.Core.Infastructure;
using ExtendedReports.Data;
using ExtendedReports.Data.Scheme;
using ExtendedReports.UI.Models;
using ExtendedReports.UI.Models.ResponseTypes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ExtendedReports.UI.Models.DisplayModels;
using Newtonsoft;
using Microsoft.Extensions.Configuration;
using ExtendedReports.UI.Models.RequestModel;
using ExtendedReports.UI.Models.ExtraClasses;
using ExtendedReports.UI.Helpers;
using ExtendedReports.Data.LocalDb;
using ExtendedReports.Data.Models.DB;
using Microsoft.EntityFrameworkCore;

namespace ExtendedReports.UI.Controllers
{
    public class ReportController : Controller
    {
        private static Context context = new Context();
        private static ExtendedReportsContext Dbcontext= new ExtendedReportsContext();
        IConfiguration iconfiguration;
        private IReportRepository reportRepo;
        private string localPath;

        private static Context Context
        {
            get { return context; }

        }
        public ReportController(IReportRepository _reportRepo, IConfiguration _iconfiguration)
        {
            reportRepo = _reportRepo;
            iconfiguration = _iconfiguration;
            localPath = iconfiguration.GetSection("Settings").GetSection("CopyFromPath").Value;
       
        }

        public IActionResult Index()
        {
            if (Dbcontext.ReadLines.Count() == 0)
            {
                FillContext(true);
            }
            List<SelectListItem> customerList = new List<SelectListItem>();
            customerList.Add(new SelectListItem { Selected = true, Text = "Lütfen bir müşteri seçiniz.", Value = "-1" });
            List<SelectListItem> deviceList = new List<SelectListItem>();
            deviceList.Add(new SelectListItem { Selected = true, Text = "Lütfen bir ip seçiniz.", Value = "-1" });
            foreach (var device in Dbcontext.Devices.AsNoTracking())
            {
                deviceList.Add(new SelectListItem
                {
                    Text = device.DeviceIp,
                    Selected = false,
                    Value = device.DeviceIp /*+ "-" + device.relatedCustomerId.ToString()*/
                });
            }
            foreach (var customer in Dbcontext.Customers.AsNoTracking())
            {
                customerList.Add(new SelectListItem
                {
                    Text = customer.CustomerName,
                    Selected = false,
                    Value = customer.Id.ToString()
                });
            }
            ReportModel model = new ReportModel();
            model.customers = customerList;
            model.devices = deviceList;
            model.context = Dbcontext;

            return View(model);
        }

        [HttpGet]
        public IActionResult ReportPaper(string customerId, bool forCustomerAvr, bool forIps, string[] ips)
        {
            if (forCustomerAvr)
            {
                var datas = (from x in Dbcontext.ReadLines.AsNoTracking()
                             where x.CustomerName == Dbcontext.Customers.AsNoTracking().FirstOrDefault(cust => cust.Id == Convert.ToInt64(customerId)).CustomerName
                             group new { x.Date, x.DeviceIp, x.Value } by x.Date
                        ).Select(x => new DateDataModel { Date = x.Key, Data = x.Sum(k => k.Value) });
                var chartResult = FillDate.CheckDatesForCustomerAVR(Dbcontext.ReadLines.AsNoTracking().OrderBy(X=>X.Date).FirstOrDefault().Date, Dbcontext.ReadLines.OrderBy(X => X.Date).LastOrDefault().Date, datas.ToList());
                return Json(chartResult);
            }
            else if (forIps)
            {
                var chartResult = FillDate.CheckDatesForSelectedIps(Dbcontext.ReadLines.AsNoTracking().OrderBy(x=>x.Date).FirstOrDefault().Date, Dbcontext.ReadLines.AsNoTracking().OrderBy(x=>x.Date).LastOrDefault().Date, ips, Dbcontext);
                return Json(chartResult);
            }

            else
            {
                return null;
            }


        }



        public IActionResult CustomerPort()
        {
            if (Dbcontext.ReadLines == null)
            {
                FillContext(true);
            }
            List<SelectListItem> customerList = new List<SelectListItem>();
            customerList.Add(new SelectListItem { Selected = true, Text = "Lütfen bir müşteri seçiniz.", Value = "-1" });
            List<SelectListItem> deviceList = new List<SelectListItem>();
            deviceList.Add(new SelectListItem { Selected = true, Text = "Lütfen bir ip seçiniz.", Value = "-1" });
            foreach (var device in Dbcontext.Devices)
            {
                deviceList.Add(new SelectListItem
                {
                    Text = device.DeviceIp,
                    Selected = false,
                    Value = device.DeviceIp
                });
            }

            foreach (var customer in Dbcontext.Customers)
            {
                customerList.Add(new SelectListItem
                {
                    Text = customer.CustomerName,
                    Selected = false,
                    Value = customer.Id.ToString()
                });
            }
            
            ReportModel model = new ReportModel();
            model.customers = customerList;
            model.devices = deviceList;
            model.context = Dbcontext;

            return View(model);
        }

        [HttpPost]
        public JsonResult CustomerPort([FromBody] ReportRequestModel model)
        {
            if (model.forCustomerAvr)
            {
                var portReports = FillDate.CheckDatesForCustomersPort(Dbcontext.ReadLines.OrderBy(x => x.Date).FirstOrDefault().Date, Dbcontext.ReadLines.OrderBy(x => x.Date).LastOrDefault().Date, model.customerId, Dbcontext);
                return Json(portReports);
                

            }
            else if (model.forIps)
            {
                var portReports = FillDate.CheckDatesForIpsPort(Dbcontext.ReadLines.OrderBy(x=>x.Date).FirstOrDefault().Date, Dbcontext.ReadLines.OrderBy(x => x.Date).LastOrDefault().Date,model.ips,Dbcontext);
                return Json(portReports);
            }
            else
            {
                return null;
            }
        }

        [HttpDelete]
        public bool EmptyTargetDirectory([FromBody] string copyTo)
        {
            try
            {
                reportRepo.clearPath(copyTo);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        //public void UpdateDB()
        //{
        //    reportRepo.updateDB();
        //}

        public IActionResult TruncateAndFillDB()
        {
            FillContext(true);
            //reportRepo.TruncateTable();
            //reportRepo.fillDB(localPath);

            return RedirectToAction("Index");
        }

        public IActionResult UpdateDB()
        {
            reportRepo.updateDB(localPath);
            return RedirectToAction("Index");
        }


        public void FillContext(bool isGonnaReset)
        {
            string path = localPath;
            if (isGonnaReset == true)
            {
                reportRepo.TruncateTable();
                var isSuccesfull = reportRepo.fillDB(path);
                var first = Dbcontext.ReadLines.OrderBy(x => x.Date).FirstOrDefault().Date;
                var last = Dbcontext.ReadLines.OrderBy(x => x.Date).LastOrDefault().Date;
                context.datesAndDatas = FillDate.FillDateForTheBeginning(first, last);
            }
            else
            {
                var isSuccesfull = reportRepo.fillDB(path);
                var first = Dbcontext.ReadLines.OrderBy(x => x.Date).FirstOrDefault().Date;
                var last = Dbcontext.ReadLines.OrderBy(x => x.Date).LastOrDefault().Date;
                context.datesAndDatas = FillDate.FillDateForTheBeginning(first, last);
            }

        }

        [HttpGet]
        public JsonResult GetData([FromQuery] string customerName, string deviceIp, DateTime? dataTime)
        {
            List<ReadLines> selectedDatas = new List<ReadLines>();
            if (customerName != null)
            {
                selectedDatas = Dbcontext.ReadLines.Where(customer => customer.CustomerName == customerName).ToList();
            }
            else if (deviceIp != null)
            {
                selectedDatas = selectedDatas.Where(device => device.DeviceIp == deviceIp).ToList();
            }
            else if (dataTime != null)
            {
                selectedDatas = selectedDatas.Where(date => date.Date == dataTime).ToList();
            }
            else
            {
                return Json(new ApiResponse { code = 204, message = "Service can not return the whole data, please chose one filter." });
            }

            return Json(new ApiResponse { code = 200, data = selectedDatas, message = "Data filtered and was returned." });
        }
    }
}