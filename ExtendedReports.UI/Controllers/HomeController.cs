﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ExtendedReports.UI.Models;
using ExtendedReports.Core.Infastructure;
using Microsoft.Extensions.Configuration;

namespace ExtendedReports.UI.Controllers
{
    public class HomeController : Controller
    {
        private IReportRepository repo;
        IConfiguration iconfiguration;
        public HomeController(IReportRepository _repo, IConfiguration _iconfiguration)
        {
            repo = _repo;
            iconfiguration = _iconfiguration;
        }
        public IActionResult Index()
        {
            string value1 = iconfiguration.GetSection("Settings").GetSection("CopyFromPath").Value;
            //repo.readDatas(@"C:\Users\Tuba\source\repos\ExtendedReports\ExtendedReports.UI\data");
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
