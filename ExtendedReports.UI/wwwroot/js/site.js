﻿
function loadContext() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Report/FillContext",
        data: null,
        success: function (result) {
            var customerDiv = document.getElementById('customer');
            customerDiv.style.display = 'block';
            var devicesDiv = document.getElementById('devices');
            customerDiv.style.display = 'block';
        },
        error: function (xmlhttprequest, textstatus, errorthrown) {
            alert(" conection to the server failed ");
            console.log("error: " + errorthrown);
        }
    });
}
function filterData() {
    var customerName = $("#customer option:selected").text();
    var customerNo = $("#customer").val();
    var deviceIp = $("#devices option:selected");
    var deviceIpList = [];
    var deviceNames = [];
    for (var i = 0; i < deviceIp.length; i++) {
        deviceIpList.push(deviceIp[i].innerHTML);
        deviceNames.push(deviceIp[i].getAttribute("id"));
    }
    if (customerNo != -1) {
        var customerDiv = document.getElementById('customerReport');
        customerDiv.style.display = 'block';
        var divs = document.getElementById("charts").children;
        for (var i = 0; i < divs.length - 1; i++) {
            var name = "main+" + i;
            var customerDiv = document.getElementById('main+' + i);
            customerDiv.style.display = 'none';
        }
        var obj = {};
        obj.customerId = customerNo;
        obj.forCustomerAvr = true;
        obj.forIps = false;
        obj.ips = null;
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            
            url: "/Report/ReportPaper?customerId=" + customerNo + "&forCustomerAvr=true&forIps=false&ips=null",
            data: JSON.stringify(obj),
            success: function (result) {
                $("#data").innerHTML = result;
                console.log(result.datas);
                drawChart(result.datas, result.dates, result.customerName, null, "customer");
            },
            error: function (xmlhttprequest, textstatus, errorthrown) {
                alert(" conection to the server failed ");
                console.log("error: " + errorthrown);
            }
        });

    }
    else if (deviceIp != -1) {
        console.log("geldi");
        var customerDiv = document.getElementById('customerReport');
        customerDiv.style.display = 'none';
        var divs = document.getElementById('charts').children;
        for (var i = 1; i < deviceIp.length + 1; i++) {
            divs[i].style.display = 'block';
        }
        var obj = {};
        obj.customerId = null;
        obj.forCustomerAvr = false;
        obj.forIps = true;
        obj.ips = deviceIpList;
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "Report/ReportPaper?customerId=null&forCustomerAvr=false&forIps=true&ips=" + obj.ips + "",
            data: JSON.stringify(obj),
            success: function (result) {
                console.log(result);
                var series = [];
                for (var i = 0; i < result.charts.length; i++) {
                    var obj = {};
                    obj.name = result.charts[i].ip;
                    obj.type = 'line';
                    obj.smooth = true;
                    obj.symbol = 'none';
                    obj.sampling = 'average';
                    obj.itemStyle = {
                        color: 'rgb(255, 70, 131)'
                    };
                    obj.areaStyle = {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgb(255, 158, 68)'
                        }, {
                            offset: 1,
                            color: 'rgb(255, 70, 131)'
                        }])
                    };
                    obj.data = result.charts[i].datas;
                    series.push(obj);

                }
                drawChart(result.charts, result.charts, result.customerName, null, "ips", series);
            },
            error: function (xmlhttprequest, textstatus, errorthrown) {
                alert(" conection to the server failed ");
                console.log("error: " + errorthrown);
            }
        });
    }
    else {
        $("#alertBox").show();
    }


}
function drawChart(data, date, customerName, deviceIp, type, multiSet) {
    var option = {};

    if (type == "customer") {
        var myChart = echarts.init(document.getElementById('customerReport'));
        option = {
            tooltip: {
                trigger: 'axis',
                position: function (pt) {
                    return [pt[0], '100%'];
                }
            },
            title: {
                left: 'center',
                text: customerName,
            },
            toolbox: {
                feature: {
                    dataZoom: {
                        yAxisIndex: 'none'
                    },
                    restore: {},
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: date
            },
            yAxis: {
                type: 'value',
                boundaryGap: [0, '1%']
            },
            dataZoom: [{
                type: 'inside',
                start: 0,
                end: 100
            }, {
                start: 0,
                end: 100,
                handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                handleSize: '80%',
                handleStyle: {
                    color: '#fff',
                    shadowBlur: 3,
                    shadowColor: 'rgba(0, 0, 0, 0.6)',
                    shadowOffsetX: 2,
                    shadowOffsetY: 2
                }
            }],
            series: [
                {
                    name: 'Tüketilen Enerji',
                    type: 'line',
                    smooth: true,
                    symbol: 'none',
                    sampling: 'average',
                    itemStyle: {
                        color: 'rgb(255, 70, 131)'
                    },
                    areaStyle: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgb(255, 158, 68)'
                        }, {
                            offset: 1,
                            color: 'rgb(255, 70, 131)'
                        }])
                    },
                    data: data
                }
            ]
        };

        myChart.setOption(option);
    }
    else {
        for (var i = 0; i < multiSet.length; i++) {
            var name = "main+" + i;
            var myChart = echarts.init(document.getElementById(name));
            var option = {
                tooltip: {
                    trigger: 'axis',
                    position: function (pt) {
                        return [pt[0], '10%'];
                    }
                },
                title: {
                    left: 'center',
                    text: multiSet[i].ip,
                },
                toolbox: {
                    feature: {
                        dataZoom: {
                            yAxisIndex: 'none'
                        },
                        restore: {},
                        saveAsImage: {}
                    }
                },
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: date[i].dates
                },
                yAxis: {
                    type: 'value',
                    boundaryGap: [0, '100%']
                },
                dataZoom: [{
                    type: 'inside',
                    start: 0,
                    end: 10
                }, {
                    start: 0,
                    end: 10,
                    handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                    handleSize: '80%',
                    handleStyle: {
                        color: '#fff',
                        shadowBlur: 3,
                        shadowColor: 'rgba(0, 0, 0, 0.6)',
                        shadowOffsetX: 2,
                        shadowOffsetY: 2
                    }
                }],
                series: multiSet[i]
            };

            myChart.setOption(option);
        }
    }
}
function drawChartForPort(data, date, customerName, deviceIp, type, multiSet) {
    var option = {};

    if (type == "customer") {
        console.log("data");
        console.log(data);
        console.log("date");
        console.log(date);
        var myChart = echarts.init(document.getElementById('customerReport'));
        option = {
            tooltip: {
                trigger: 'axis',
                position: function (pt) {
                    return [pt[0], '10%'];
                }
            },
            title: {
                left: 'center',
                text: customerName,
            },
            toolbox: {
                feature: {
                    dataZoom: {
                        yAxisIndex: 'none'
                    },
                    restore: {},
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: date
            },
            yAxis: {
                type: 'value',
                boundaryGap: [0, '10%']
            },
            dataZoom: [{
                type: 'inside',
                start: 0,
                end: 100
            }, {
                start: 0,
                end: 10,
                handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                handleSize: '80%',
                handleStyle: {
                    color: '#fff',
                    shadowBlur: 3,
                    shadowColor: 'rgba(0, 0, 0, 0.6)',
                    shadowOffsetX: 2,
                    shadowOffsetY: 2
                }
                }],
            series: [
                {
                    name: 'Girdi Sayısı: ',
                    type: 'line',
                    smooth: true,
                    symbol: 'none',
                    sampling: 'average',
                    itemStyle: {
                        color: 'rgb(255, 70, 131)'
                    },
                    areaStyle: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgb(255, 158, 68)'
                        }, {
                            offset: 1,
                            color: 'rgb(255, 70, 131)'
                        }])
                    },
                    data: data
                }
            ]
        };

        myChart.setOption(option);
    }
    else {
        console.log("multi");
        console.log(multiSet);
        console.log("date");
        console.log(date);
        for (var i = 0; i < multiSet.length; i++) {
            var name = "main+" + i;
            var myChart = echarts.init(document.getElementById(name));
            var option = {
                tooltip: {
                    trigger: 'axis',
                    position: function (pt) {
                        return [pt[0], '10%'];
                    }
                },
                title: {
                    left: 'center',
                    text: customerName[i]+"-"+deviceIp[i],
                },
                toolbox: {
                    feature: {
                        dataZoom: {
                            yAxisIndex: 'none'
                        },
                        restore: {},
                        saveAsImage: {}
                    }
                },
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: date
                },
                yAxis: {
                    type: 'value',
                    boundaryGap: [0, '100%']
                },
                dataZoom: [{
                    type: 'inside',
                    start: 0,
                    end: 10
                }, {
                    start: 0,
                    end: 10,
                    handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                    handleSize: '80%',
                    handleStyle: {
                        color: '#fff',
                        shadowBlur: 3,
                        shadowColor: 'rgba(0, 0, 0, 0.6)',
                        shadowOffsetX: 2,
                        shadowOffsetY: 2
                    }
                    }],
                series: multiSet[i]
            };

            myChart.setOption(option);
        }
    }
}
function filterDataForPortReport() {
    var customerName = $("#customer option:selected").text();
    var customerNo = $("#customer").val();
    var deviceIp = $("#devices option:selected");
  
    var deviceIpList = [];
    var deviceNames = [];
    for (var i = 0; i < deviceIp.length; i++) {
        deviceIpList.push(deviceIp[i].innerHTML);
        deviceNames.push(deviceIp[i].getAttribute("id"));
    }
    console.log("deviceİpleri");
    console.log(deviceIpList);
    if (customerNo != -1) {
        var customerDiv = document.getElementById('customerReport');
        customerDiv.style.display = 'block';
        var divs = document.getElementById("charts").children;
        for (var i = 0; i < divs.length - 1; i++) {
            var name = "main+" + i;
            var customerDiv = document.getElementById('main+' + i);
            customerDiv.style.display = 'none';
        }
        var obj = {};
        obj.customerId = customerNo;
        obj.forCustomerAvr = true;
        obj.forIps = false;
        obj.ips = null;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Report/CustomerPort",
            data: JSON.stringify(obj),
            success: function (result) {
                console.log("ded");
                console.log(result);
                var series = [];
                var customerNames = [];
                var ips = [];
             
                customerNames.push(result.charts[0].customerName);
                ips.push(result.charts[0].deviceIp );
                    var obj = {};
                obj.name = result.charts[0].deviceIp;
                    obj.type = 'line';
                    obj.smooth = true;
                    obj.symbol = 'none';
                    obj.sampling = 'average';
                    obj.itemStyle = {
                        color: 'rgb(255, 70, 131)'
                    };
                    obj.areaStyle = {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgb(255, 158, 68)'
                        }, {
                            offset: 1,
                            color: 'rgb(255, 70, 131)'
                        }])
                };
                obj.data = result.charts[0].datas;
                    series.push(obj);
                drawChartForPort(result.charts[0].datas, result.charts[0].dates, customerNames, ips, "customer", series);
            },
            error: function (xmlhttprequest, textstatus, errorthrown) {
                alert(" conection to the server failed ");
                console.log("error: " + errorthrown);
            }
        });

    }
    else if (deviceIp != -1) {
        var customerDiv = document.getElementById('customerReport');
        customerDiv.style.display = 'none';
        var divs = document.getElementById('charts').children;
        for (var i = 1; i < deviceIp.length + 1; i++) {
            divs[i].style.display = 'block';
        }
        var obj = {};
        obj.customerId = null;
        obj.forCustomerAvr = false;
        obj.forIps = true;
        obj.ips = deviceIpList;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Report/CustomerPort",
            data: JSON.stringify(obj),
            success: function (result) {
                console.log("res");
                console.log(result);
                var series = [];
                var customerNames = [];
                var ips = [];
                for (var i = 0; i < result.charts.length; i++) {
                    customerNames.push(result.customerName);
                    ips.push(result.ip);
                    var obj = {};
                    obj.name = result.charts[i].ip.toString();
                    obj.type = 'line';
                    obj.smooth = true;
                    obj.symbol = 'none';
                    obj.sampling = 'average';
                    obj.itemStyle = {
                        color: 'rgb(255, 70, 131)'
                    };
                    obj.areaStyle = {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgb(255, 158, 68)'
                        }, {
                            offset: 1,
                            color: 'rgb(255, 70, 131)'
                        }])
                    };
                    obj.data = result.charts[i].datas;
                    series.push(obj);

                }
                console.log("date 1");
                console.log(result);
                console.log(result.charts[0]);
                drawChartForPort(result, result.charts[0].dates, customerNames, ips, "ips", series);
            },
            error: function (xmlhttprequest, textstatus, errorthrown) {
                alert(" conection to the server failed ");
                console.log("error: " + errorthrown);
            }
        });
    }
    else {
        $("#alertBox").show();
    }


}

