﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExtendedReports.UI.Models.RequestModel
{
    public class ReportRequestModel
    {
        public long? customerId { get; set; }
        public List<string> ips { get; set; }
        public bool forCustomerAvr { get; set; }
        public bool forIps { get; set; }
    }
}
