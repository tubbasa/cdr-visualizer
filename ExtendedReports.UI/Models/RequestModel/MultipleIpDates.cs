﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExtendedReports.UI.Models.ExtraClasses
{
    public class MultipleIpDates
    {
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }
        public long customerNo { get; set; }
        public string ip { get; set; }
    }
}
