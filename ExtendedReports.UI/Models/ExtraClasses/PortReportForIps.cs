﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExtendedReports.UI.Models.ExtraClasses
{
    public class PortReportForIps
    {
        public int count { get; set; }
        public DateTime? datetime { get; set; }
        public string Ip { get; set; }
        public long? customerNo { get; set; }
        public string customerName { get; set; }
    }
}
