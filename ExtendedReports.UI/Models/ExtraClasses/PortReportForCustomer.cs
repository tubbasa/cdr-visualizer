﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExtendedReports.UI.Models.ExtraClasses
{
    public class PortReportForCustomer
    {
        public int count{ get; set; }
        public DateTime? Datetime{ get; set; }
        public long? customerNo { get; set; }
        public string customerName { get; set; }
    }
}
