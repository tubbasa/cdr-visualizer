﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExtendedReports.UI.Models.ResponseTypes
{
    public class ApiResponse
    {
        public int code { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }
}
