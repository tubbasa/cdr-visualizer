﻿using ExtendedReports.Data.Models.DB;
using ExtendedReports.Data.Scheme;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExtendedReports.UI.Models.DisplayModels
{
    public class ReportModel
    {
        public List<SelectListItem> customers { get; set; }
        public List<SelectListItem> devices { get; set; }
        public ExtendedReportsContext context { get; set; }

    }
}
