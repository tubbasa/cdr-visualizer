﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExtendedReports.UI.Models
{
    public class DateDataModel
    {
        public DateTime? Date { get; set; }
        public decimal? Data { get; set; }
        public string customerName { get; set; }
        public string deviceIp { get; set; }
    }
}
