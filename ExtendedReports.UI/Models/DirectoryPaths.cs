﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExtendedReports.UI.Models
{
    public class DirectoryPaths
    {
        public string copyFrom { get; set; }
        public string copyTo{ get; set; }
    }
}
