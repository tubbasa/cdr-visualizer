﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace ExtendedReports.Core.ExtensionMethods
{
    public static class GetConnectionString
    {
        public static string GetConnectionStringFromAppJSON()
        {
            //C:\\Users\\Tuba\\source\\Workspaces\\Tools.PDU.CDRVisualizer\\ExtendedReports.UI
            //C:\inetpub\gebze-cdr
            JObject o1 = JObject.Parse(File.ReadAllText(@"M:\\Projects\\source\\Workspaces\\CDR-Visualizer\\ExtendedReports.UI\\appsettings.json"));
            var connstr = "";
            // read JSON directly from a file
            using (StreamReader file = File.OpenText(@"M:\\Projects\\source\\Workspaces\\CDR-Visualizer\\ExtendedReports.UI\\appsettings.json"))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JObject o2 = (JObject)JToken.ReadFrom(reader);
                var conn = o2.GetValue("ConnectionStrings");

                foreach (JProperty child in conn.Children<JProperty>())
                {
                    connstr = (string)child.Value;
                }

            }
            return connstr;
            //JObject obj = JObject.Parse(jsonString);
            //var recList = obj.SelectTokens("$..ReviewStatistics.RecommendedCount").ToList();
            //var totalList = obj.SelectTokens("$..ReviewStatistics.TotalReviewCount").ToList();
        }
    }

}
