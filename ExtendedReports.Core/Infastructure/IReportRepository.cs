﻿using ExtendedReports.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExtendedReports.Core.Infastructure
{
    public interface IReportRepository:IRepository<Report>
    {
    }
}
