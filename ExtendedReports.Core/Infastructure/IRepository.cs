﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtendedReports.Core.Infastructure
{
    public interface IRepository<T> where T : class
    {
        T readDatas(string path);
        bool clearPath(string path);
        bool uploadFiles(string copyFrom, string copyTo);
        string changeDateTimeSymbol(string dateTime);
        bool fillDB(string path);
        bool TruncateTable();
        bool updateDB(string path);
    }
}
