﻿using ExtendedReports.Core.Infastructure;
using ExtendedReports.Data;
using ExtendedReports.Data.Models.DB;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using static System.Net.Mime.MediaTypeNames;

namespace ExtendedReports.Core.Repositories
{
    public class ReportRepository : IReportRepository
    {
        ExtendedReportsContext context;
        public ReportRepository()
        {
            context =new ExtendedReportsContext();
        }
     
        string connectionString = ExtendedReports.Core.ExtensionMethods.GetConnectionString.GetConnectionStringFromAppJSON();

        #region deleteFileFromSelectedPath
        public bool clearPath(string path)
        {
            try
            {

                System.IO.DirectoryInfo di = new DirectoryInfo(path);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }


                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        #endregion
        #region readDatas
        public Report readDatas(string path)
        {
            var directory = Directory.GetCurrentDirectory();
            DirectoryInfo d = new DirectoryInfo(path);
            FileInfo[] Files = d.GetFiles("*.txt");
            Report report = new Report { report = new List<ReportLine>() };
            foreach (FileInfo file in Files)
            {


                using (var fileStream = File.OpenRead(file.FullName))
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, 100))
                {
                    var dd = file.Name;
                    var ff = file.Name.Split('_')[1].Split('-')[0];
                    var year = ff.Substring(0, 4);
                    var month = ff.Substring(4, 2);
                    var day = ff.Substring(6, 2);
                    var hour = ff.Substring(8, 2);
                    string line;
                    while ((line = streamReader.ReadLineAsync().Result) != null)
                    {
                        var splittedLine = line.Split("||");
                        ReportLine reportLine = new ReportLine();
                        reportLine.dataTime = Convert.ToDateTime(day + "/" + month + "/" + year + " " + hour + ":00");
                        reportLine.customerName = splittedLine[1].Split("|")[0];
                        reportLine.deviceIp = splittedLine[1].Split("|")[1];
                        reportLine.customerNumber = Convert.ToInt64(splittedLine[2].Split("|")[1]);
                        reportLine.amount = Convert.ToInt32(splittedLine[2].Split("|")[2]);
                        report.report.Add(reportLine);
                    }
                }


            }

            return report;
        }
        #endregion
        #region uploadFiles
        public bool uploadFiles(string copyFrom, string copyTo)
        {
            try
            {
                var mainDirectory = Directory.GetCurrentDirectory();
                System.IO.DirectoryInfo files = new DirectoryInfo(copyFrom);
                //C: \Users\Tuba\Desktop\TurkcellDataEdition\TurkcellDataEdition\CDR
                foreach (var item in files.GetFiles())
                {
                    var source = Path.Combine(copyFrom, item.Name);
                    var destination = Path.Combine(mainDirectory + @"\" + copyTo, item.Name);
                    File.Copy(source, destination);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string changeDateTimeSymbol(string dateTime)
        {
            return dateTime.Replace('/', '-');
        }
        #endregion
        #region fillDB
        public bool fillDB(string path)
        {
            context = new ExtendedReportsContext();
            try
            {
                // context.FileDetails.AsTracking<FileDetails>();
                var directory = Directory.GetCurrentDirectory();
                DirectoryInfo d = new DirectoryInfo(path);
                FileInfo[] Files = d.GetFiles("*.txt");
                List<ReadLines> lineList = new List<ReadLines>();
                SqlConnection conn = new SqlConnection(connectionString);
                foreach (FileInfo file in Files)
                {
                    FileDetails fileDetail = new FileDetails();
                    fileDetail.CreatedTime = file.CreationTime;
                    fileDetail.LastEditedDate = file.LastWriteTime;
                    fileDetail.FileName = file.FullName;
             
                    if (context.FileDetails.AsNoTracking().Any(u => u.FileName == fileDetail.FileName)==false)
                    {
                        context.FileDetails.Add(fileDetail);
                        //context.Entry<FileDetails>(fileDetail);
                    
                    }
                }
                   context.SaveChanges();
                foreach (var file in context.FileDetails.AsNoTracking())
                {
                    using (var fileStream = File.OpenRead(file.FileName))
                    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, 100))
                    {
                        var dd = file.FileName;
                        var ff = file.FileName.Split('_')[1].Split('-')[0];
                        var year = ff.Substring(0, 4);
                        var month = ff.Substring(4, 2);
                        var day = ff.Substring(6, 2);
                        var hour = ff.Substring(8, 2);
                        string line;
                        while ((line = streamReader.ReadLine()) != null)
                        {
                            var splittedLine = line.Split("||");
                            ReadLines readLines = new ReadLines();
                            readLines.Date = Convert.ToDateTime(day + "/" + month + "/" + year + " " + hour + ":00");
                            //var customerName = splittedLine[1].Split("|")[0];
                            readLines.CustomerName = String.Join("", splittedLine[1].Split("|")[0].Normalize(NormalizationForm.FormD)
        .Where(c => char.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark));
                            readLines.DeviceIp = splittedLine[1].Split("|")[1];
                            readLines.CustomerNo = Convert.ToInt64(splittedLine[2].Split("|")[1]);
                            readLines.Value = Convert.ToInt32(splittedLine[2].Split("|")[2]);
                            readLines.FileId = file.Id;
                            lineList.Add(readLines);
                            #region 
                            #endregion
                            try
                            {
                                SqlCommand cmd = new SqlCommand("exec usp_InsertLine @CustomerName,@CustomerNo,@Date,@DeviceIp,@Value,@FileId", conn);
                                cmd.Parameters.AddWithValue("@CustomerName", readLines.CustomerName);
                                cmd.Parameters.AddWithValue("@CustomerNo", readLines.CustomerNo);
                                cmd.Parameters.AddWithValue("@Date", readLines.Date);
                                cmd.Parameters.AddWithValue("@DeviceIp", readLines.DeviceIp);
                                cmd.Parameters.AddWithValue("@Value", readLines.Value);
                                cmd.Parameters.AddWithValue("@FileId", readLines.FileId);
                                if (conn.State == System.Data.ConnectionState.Closed) conn.Open();
                                cmd.ExecuteNonQuery();

                            }
                            catch (Exception exp)
                            {


                            }

                        }
                    }


                }

                #region fillDevicesAndCustomers
                context.ReadLines.GroupBy(x => x.CustomerName).ToList().ForEach(x =>
                {
                    context.Customers.Add(new Customers { CustomerName = x.Key });
                });

                context.ReadLines.GroupBy(x => x.DeviceIp).ToList().ForEach(x =>
                {
                    context.Devices.Add(new Devices { DeviceIp = x.Key });
                });
                var res = context.SaveChanges();
                #endregion

                return true;
            }
            catch (Exception exp)
            {
                return true;
            }
        }
        #endregion
        #region updateDB
        public bool updateDB(string path)
        {
            try
            {
                var lastEditedFiles = context.FileDetails.OrderBy(x => x.LastEditedDate).ToList();
                var directory = Directory.GetCurrentDirectory();
                DirectoryInfo d = new DirectoryInfo(path);
                FileInfo[] Files = d.GetFiles("*.txt");
                List<ReadLines> lineList = new List<ReadLines>();
                SqlConnection conn = new SqlConnection(connectionString);
                var sayac = 0;
                foreach (FileInfo file in Files)
                {

                    FileDetails fileDetail = new FileDetails();
                    fileDetail.CreatedTime = file.CreationTime;
                    fileDetail.LastEditedDate = file.LastWriteTime;
                    fileDetail.FileName = file.FullName;
                    var isExist =
                        context.FileDetails.FirstOrDefault(x => x.FileName == fileDetail.FileName);
                    if (isExist == null)
                    {
                        context.FileDetails.Add(fileDetail);
                        context.SaveChanges();
                        using (var fileStream = File.OpenRead(file.FullName))
                        using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, 100))
                        {
                            var dd = file.Name;
                            var ff = file.Name.Split('_')[1].Split('-')[0];
                            var year = ff.Substring(0, 4);
                            var month = ff.Substring(4, 2);
                            var day = ff.Substring(6, 2);
                            var hour = ff.Substring(8, 2);
                            string line;
                            while ((line = streamReader.ReadLine()) != null)
                            {
                                var splittedLine = line.Split("||");
                                ReadLines readLines = new ReadLines();
                                readLines.Date = Convert.ToDateTime(day + "/" + month + "/" + year + " " + hour + ":00");
                                //var customerName = splittedLine[1].Split("|")[0];
                                readLines.CustomerName = String.Join("", splittedLine[1].Split("|")[0].Normalize(NormalizationForm.FormD)
            .Where(c => char.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark));
                                readLines.DeviceIp = splittedLine[1].Split("|")[1];
                                readLines.CustomerNo = Convert.ToInt64(splittedLine[2].Split("|")[1]);
                                readLines.Value = Convert.ToInt32(splittedLine[2].Split("|")[2]);
                                readLines.FileId = fileDetail.Id;




                                //context.ReadLines.Add(readLines);
                                try
                                {
                                    //var resu = context.ReadLines.FromSql($"exec usp_InsertLine @CustomerName={readLines.CustomerName},@CustomerNo={readLines.CustomerNo},@Date={readLines.Date},@DeviceIp={readLines.DeviceIp},@Value={readLines.Value}").First();

                                    SqlCommand cmd = new SqlCommand("exec usp_InsertLine @CustomerName,@CustomerNo,@Date,@DeviceIp,@Value,@FileId", conn);
                                    cmd.Parameters.AddWithValue("@CustomerName", readLines.CustomerName);
                                    cmd.Parameters.AddWithValue("@CustomerNo", readLines.CustomerNo);
                                    cmd.Parameters.AddWithValue("@Date", readLines.Date);
                                    cmd.Parameters.AddWithValue("@DeviceIp", readLines.DeviceIp);
                                    cmd.Parameters.AddWithValue("@Value", readLines.Value);
                                    cmd.Parameters.AddWithValue("@FileId", readLines.FileId);
                                    if (conn.State == System.Data.ConnectionState.Closed) conn.Open();
                                    cmd.ExecuteNonQuery();

                                }
                                catch (InvalidOperationException exp)
                                {


                                }
                            }
                        }
                    }

                    else
                    {
                            isExist.LastEditedDate = fileDetail.LastEditedDate;
                            context.FileDetails.Update(isExist);
                            context.SaveChanges();
                            using (var fileStream = File.OpenRead(file.FullName))
                            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, 100))
                            {
                                var query = "Delete from ReadLines where FileId='" + isExist.Id + "'";
                                var willBeDeletedLines = context.ReadLines.Where(x=>x.FileId==isExist.Id);
                                context.ReadLines.RemoveRange(willBeDeletedLines);
                                context.SaveChanges();
                                var dd = file.Name;
                                var ff = file.Name.Split('_')[1].Split('-')[0];
                                var year = ff.Substring(0, 4);
                                var month = ff.Substring(4, 2);
                                var day = ff.Substring(6, 2);
                                var hour = ff.Substring(8, 2);
                                string line;
                                while ((line = streamReader.ReadLine()) != null)
                                {
                                    var splittedLine = line.Split("||");
                                    ReadLines readLines = new ReadLines();
                                    readLines.Date = Convert.ToDateTime(day + "/" + month + "/" + year + " " + hour + ":00");
                                    readLines.CustomerName = String.Join("", splittedLine[1].Split("|")[0].Normalize(NormalizationForm.FormD)
                .Where(c => char.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark));
                                    readLines.DeviceIp = splittedLine[1].Split("|")[1];
                                    readLines.CustomerNo = Convert.ToInt64(splittedLine[2].Split("|")[1]);
                                    readLines.Value = Convert.ToInt32(splittedLine[2].Split("|")[2]);
                                    readLines.FileId = isExist.Id;
                                    try
                                    {
                                        SqlCommand cmd = new SqlCommand("exec usp_InsertLine @CustomerName,@CustomerNo,@Date,@DeviceIp,@Value,@FileId", conn);
                                        cmd.Parameters.AddWithValue("@CustomerName", readLines.CustomerName);
                                        cmd.Parameters.AddWithValue("@CustomerNo", readLines.CustomerNo);
                                        cmd.Parameters.AddWithValue("@Date", readLines.Date);
                                        cmd.Parameters.AddWithValue("@DeviceIp", readLines.DeviceIp);
                                        cmd.Parameters.AddWithValue("@Value", readLines.Value);
                                        cmd.Parameters.AddWithValue("@FileId", readLines.FileId);
                                        if (conn.State == System.Data.ConnectionState.Closed) conn.Open();
                                        cmd.ExecuteNonQuery();

                                    }
                                    catch (InvalidOperationException exp)
                                    {
                                        
                                    }
                                    

                                }
                            }

                    }
                    sayac++;
                }

                
                #region fillDevicesAndCustomers
                context.ReadLines.GroupBy(x => x.CustomerName).ToList().ForEach(x =>
                {
                    context.Customers.Add(new Customers { CustomerName = x.Key });
                });

                context.ReadLines.GroupBy(x => x.DeviceIp).ToList().ForEach(x =>
                {
                    context.Devices.Add(new Devices { DeviceIp = x.Key });
                });
                var res = context.SaveChanges();
                #endregion

                return true;
            }
            catch (Exception exp)
            {
                return true;
            }
        }
        #endregion
        #region TruncateDB
        public bool TruncateTable()
        {
            //context.Database.ExecuteSqlCommand("delete from FileDetails");
            //context.Database.ExecuteSqlCommand("delete from ReadLines");
            //context.Database.ExecuteSqlCommand("delete from Customers");
            //context.Database.ExecuteSqlCommand("delete from Devices");

            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [FileDetails]");
            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [ReadLines]");
            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Customers]");
            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Devices]");
            var result = context.SaveChanges();
           return result == 1 ? true : false;
        }

        #endregion
    }
}
